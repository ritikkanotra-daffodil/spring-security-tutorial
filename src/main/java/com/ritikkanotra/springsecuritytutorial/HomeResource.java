package com.ritikkanotra.springsecuritytutorial;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeResource {

    @GetMapping("/")
    public String home() {
        return ("<h1>Welcome HOME</h1>");
    }

    @GetMapping("/user")
    public String user() {
        return ("<h1>Welcome HOME ::[USER]::</h1>");
    }

    @GetMapping("/admin")
    public String admin() {
        return ("<h1>Welcome HOME ::[ADMIN]::</h1>");
    }

}
